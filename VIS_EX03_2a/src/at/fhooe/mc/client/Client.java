package at.fhooe.mc.client;

/**
 * This class contains the Client who can access the HelloWorld web service, get data from it and the print that to the console.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import at.fhooe.mc.generated.DummyData;
import at.fhooe.mc.generated.HelloWorld;
import at.fhooe.mc.generated.HelloWorldService;

public class Client {
    public static void main(String[] _argv) {

        //connect to the service
        HelloWorldService srv = new HelloWorldService();
        HelloWorld soap = srv.getHelloWorldPort();

        //get the data & print it
        System.out.println("server --> " + soap.saySomething());

        //parameter is purely nonsensical
        DummyData dummyData = soap.getData("data");
        System.out.println("server text: " + dummyData.getMText());
        System.out.println("server time: " + dummyData.getMTime());
    } // main
}