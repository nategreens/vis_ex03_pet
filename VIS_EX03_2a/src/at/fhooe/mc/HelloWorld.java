package at.fhooe.mc;

/**
 * This class contains the Web Service HelloWorld which can be reached through the Client.
 * It holds two methods that both return different text which can be printed to the clients console.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class HelloWorld {


    /**
     * This method returns "Hello World!" if the client calls it.
     *
     * @return a string containing text that can be printed to the client's console
     */
    @WebMethod
    public String saySomething() {
        return "Hello World!";
    }

    /**
     * This method returns text to the client.
     *
     * @param _name This parameter serves no purpose, but it holds the opportunity to return different data to the client depending on input
     * @return a string
     */
    @WebMethod
    public DummyData getData(String _name) {
        return new DummyData("Dies ist ein Test", System.currentTimeMillis());

    }
}
