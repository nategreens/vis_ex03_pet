package at.fhooe.mc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class holds the DummyData Object which consists of a String holding text and a long holding the time.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */


@XmlRootElement
public class DummyData {

    @XmlElement
    String mText;

    @XmlElement
    long mTime;

    /**
     * standardconstructor
     */
    public DummyData() {
    }

    /**
     * constructor
     *
     * @param _txt  a String containing text
     * @param _time a long containing the time
     */
    public DummyData(String _txt, long _time) {
        mText = _txt;
        mTime = _time;
    }

    /**
     * This method writes the DummyData object to a string.
     *
     * @return a String containing the contents of the DummyData Object
     */
    @Override
    public String toString() {
        return "DummyData from (" + mTime + ") --> " + mText;
    }
}
