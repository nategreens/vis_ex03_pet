package at.fhooe.mc;

/**
 * This class starts the HelloWorld Service and publishes it to the URL via endpoint.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import javax.xml.ws.Endpoint;

public class Main {

    public static void main(String[] args) {

        //instantialize web service
        HelloWorld test = new HelloWorld();

        //publish via endpoint
        Endpoint endpoint = Endpoint.publish("http://localhost:8081/HelloWorld", test);

        //status message for the user
        System.out.println("Server up and running...");

    }
}
