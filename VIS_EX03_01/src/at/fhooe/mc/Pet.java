package at.fhooe.mc;

/**
 * This class contains the Pet object which consists of Name, Nickname, ID, Birthday, Type & Vaccinations.
 * All elements are annotated using xml according to the requirements listed in "Uebungszettel-VIS-03.pdf".
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import javax.xml.bind.annotation.*;
import java.util.Date;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"mName", "mTyp", "mID", "mBirthday", "mVaccinations"})
public class Pet {

    @XmlElement(name = "name")
    String mName;

    @XmlAttribute(name = "nickname")
    String mNickName;

    @XmlElement(name = "id")
    String mID;

    @XmlElement(name = "birthday")
    Date mBirthday;

    @XmlElement(name = "type", namespace = "at.fhooe.mc.type")
    Type mTyp;

    @XmlElementWrapper(name = "vaccinations")
    @XmlElement(name = "vaccination")
    String[] mVaccinations;

    /**
     * Constructor
     *
     * @param mName         the animal's name
     * @param mNickName     the animal's nickname
     * @param mBirthday     the animal's birthday
     * @param mTyp          the animal's type (cat, dog, mouse or bird)
     * @param mVaccinations an array containing the animal's vaccinations
     * @param mID           the animal's identifier
     */
    public Pet(String mName, String mNickName, Date mBirthday, Type mTyp, String[] mVaccinations, String mID) {
        this.mName = mName;
        this.mNickName = mNickName;
        this.mID = mID;
        this.mBirthday = mBirthday;
        this.mTyp = mTyp;
        this.mVaccinations = mVaccinations;
    }

    /**
     * standardconstructor
     */
    public Pet() {
    }

    /**
     * This method takes a pet object and prints all the information in human readable form.
     */
    public void printPet() {

        //get the object data
        String name = this.mName;
        String nickname = this.mNickName;
        Date date = this.mBirthday;
        Type type = this.mTyp;

        //make vaccinations into string for niceness
        String[] vacc = this.mVaccinations;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < vacc.length; i++) {

            builder.append(vacc[i]);
            if (i != vacc.length - 1) {
                builder.append(",");
            }
        }
        String vaccinations = builder.toString();
        String id = this.mID;

        //print it
        System.out.println("Name: " + name);
        System.out.println("Nickname: " + nickname);
        System.out.println("Date: " + date);
        System.out.println("Type: " + type);
        System.out.println("Vaccinations: " + vaccinations);
        System.out.println("ID: " + id);


    }


}



