package at.fhooe.mc;

/**
 * This enum contains all potential types of animal (we're living in a weird world where there are only cats, dogs, mice and birds).
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */
public enum Type {
    CAT, DOG, MOUSE, BIRD
}
