package at.fhooe.mc;

/**
 * This class creates a new Pet object and prints it. Then an XML is serialized and printed, afterwards it is deserialized again and printed again.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.jaxb.xmlmodel.ObjectFactory;
import org.eclipse.persistence.oxm.MediaType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Main {

    public static void main(String[] args) throws JAXBException, ParseException {

        //Create all data for Pet object
        String inputString = "10-02-1940";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date inputDate = dateFormat.parse(inputString);
        String[] vaccinations = new String[]{"Katzenschnupfen", "Katzenseuche", "Tollwut", "Leukose"};

        //Create Pet object with data from above
        Pet pet = new Pet("Thomas", "Tom", inputDate, Type.CAT, vaccinations, "22.455.465");
        //print pet "traditionally"
        pet.printPet();


        //marshalling
        JAXBContext jc = JAXBContext.newInstance(new Class[]{Pet.class, ObjectFactory.class});
        Marshaller m = jc.createMarshaller();
        m.setProperty(MarshallerProperties.MEDIA_TYPE,
                MediaType.APPLICATION_JSON);
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter sw = new StringWriter();
        m.marshal(pet, sw);
        String data = sw.getBuffer().toString();

        //print marshalled data
        System.out.println(data);


        //unmarshalling
        Unmarshaller um = jc.createUnmarshaller();
        um.setProperty(UnmarshallerProperties.MEDIA_TYPE,
                MediaType.APPLICATION_JSON);
        Pet res = (Pet) um.unmarshal(new StringReader(data));

        //print unmarshalled data
        System.out.println(res);
    }
}