package at.fhooe.mc;

/**
 * This class contains the Jax-RS Web Service HelloWorld which publishes "Hello World" in different Texttypes to the browser.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloWorld extends Application {

    /**
     * This method prints text in html format.
     *
     * @return a String containing text in html format
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getDataHTML() {
        return "<html><head></head><body>HelloWorld (HTML text)!!!</body></html>";
    }

    /**
     * This method prints text in plain format.
     *
     * @return a String containing text in plain format
     */
    @GET
    @Path("/data")
    @Produces(MediaType.TEXT_PLAIN)
    public String getDataPlain() {
        return "HelloWorld (plaintext)";
    }

    /**
     * This method prints text in xml format.
     *
     * @return a String containing text in xml format
     */
    @GET
    @Path("/xml")
    @Produces(MediaType.TEXT_XML)
    public String getDataXML() {
        return "<HelloWorld>HelloWorld (XML text)!!!</HelloWorld>";
    }

    /**
     * This method prints text in json format.
     *
     * @return a String containing text in json format
     */
    @GET
    @Path("/json")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataJSON() {
        return "{\"val\":\"HelloWorld\"}";
    }


}
