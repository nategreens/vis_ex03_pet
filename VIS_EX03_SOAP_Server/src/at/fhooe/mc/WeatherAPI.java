package at.fhooe.mc;

import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.xmlmodel.ObjectFactory;
import org.eclipse.persistence.oxm.MediaType;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.net.URL;

/**
 * This class governs the process of deserialize XML data into Java through the unmarshaller
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */
public class WeatherAPI {
    private static String WeathersURL = "http://weathers.co/api.php?city=";

    public static Weather getWeather(String _city) throws JAXBException, IOException {

        String str = _city.replace(' ', '+');

        URL url = new URL(WeathersURL + str);
        JAXBContext context = JAXBContext.newInstance(new Class[]{Weather.class, Data.class, ObjectFactory.class});
        Unmarshaller um = context.createUnmarshaller();

        um.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        um.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, false);

        StreamSource json = new StreamSource(url.openStream());
        Weather resp = (Weather) um.unmarshal(json, Weather.class).getValue();
        return resp;
    }
}