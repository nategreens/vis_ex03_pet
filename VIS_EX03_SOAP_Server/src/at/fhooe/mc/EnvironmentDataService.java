package at.fhooe.mc;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * This class is kind of the server class for the Weather Website
 * It contains a String[] which has got every cities our Server supports
 * All elements are annotated using xml according to the requirements listed in "Uebungszettel-VIS-03.pdf".
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */
@WebService(endpointInterface = "at.fhooe.mc.IEnvService")
public class EnvironmentDataService implements IEnvService {
    public String[] cities = {"Steyr", "New York", "London", "Moscow", "Hagenberg"};

    /**
     * This method is used to get the cities array
     *
     * @return cities Array
     */
    @WebMethod
    @Override
    public String[] requestEnvironmentDataTypes() {
        return cities;
    }

    /**
     * The getter for the weather data
     *
     * @param s string containing the city
     * @return Weather EnvData
     */
    @WebMethod
    @Override
    public EnvData requestEnvironmentData(String s) {
        EnvData data = getWeatherData(s);
        System.out.print(data.toString());
        return data;
    }

    /**
     * This method returns the data for the city, which can be chosen by the user in the menu
     *
     * @param _city string containing the city
     * @return Weather EnvData
     */
    @WebMethod
    public EnvData getWeatherData(String _city) {
        EnvData envData = null;

        try {
            Weather weather = WeatherAPI.getWeather(_city);
            String weatherData = weather.getData().toString();
//            double humidity = weather.getData().getHumidity();
//            double temperature = weather.getData().getTemperature();


            envData = new EnvData(_city, weatherData, System.currentTimeMillis() / 1000);

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return envData;
    }
}
