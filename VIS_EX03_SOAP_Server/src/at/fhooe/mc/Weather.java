package at.fhooe.mc;

/**
 * This class has a data which you can access through the get Data method
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */
public class Weather {

    Data data;

    /**
     * Getter and Setter for the data
     *
     * @return data for the city
     */
    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}