package at.fhooe.mc.client;

import at.fhooe.mc.EnvData;
import at.fhooe.mc.IEnvService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Nathan Grinzinger on 19.01.2017.
 */
public class Client {
    IEnvService mSOAP;

    public Client() throws IOException {
        Service service =
                Service.create(
                        new URL("http://localhost:8081/EnvironmentData?wsdl"),
                        new QName("http://mc.fhooe.at/",
                                "EnvironmentDataServiceService"));
        mSOAP = service.getPort(IEnvService.class);
        String[] cities = mSOAP.requestEnvironmentDataTypes();
        System.out.println("Available Cities: ");
        for (int i = 0; i < cities.length; i++) {
            System.out.print(cities[i]);
            if (i != cities.length) {
                System.out.print("; ");
            }
        }
        System.out.println("Which city do you want to get the weather status from?");
        System.out.println("[0] Steyr");
        System.out.println("[1] New York");
        System.out.println("[2] London");
        System.out.println("[3] Moscow");
        System.out.println("[4] Hagenberg");
        String cityName = "a";
        int cityNumber = 6;
        cityNumber = System.in.read();
        cityNumber = cityNumber - 48;
        switch (cityNumber) {
            case 0:
                cityName = "Steyr";
                break;
            case 1:
                cityName = "NewYork";
                break;
            case 2:
                cityName = "London";
                break;
            case 3:
                cityName = "Moscow";
                break;
            case 4:
                cityName = "Hagenberg";
                break;
            default:
                cityName = "Hagenberg";
                break;
        }
        EnvData data = mSOAP.requestEnvironmentData(cityName);
        System.out.print(data.toString());
    }
}

