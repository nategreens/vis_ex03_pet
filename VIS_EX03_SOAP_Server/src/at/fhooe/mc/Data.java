package at.fhooe.mc;

import javax.xml.bind.annotation.XmlElement;

/**
 * The class data is kind of a Container which contains the getter and setter Methods for the Weather website
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */
public class Data {

    @XmlElement(name = "location")
    private String location;
    @XmlElement(name = "temperature")
    private int temperature;
    @XmlElement(name = "skytext")
    private String skytext;
    @XmlElement(name = "humidity")
    private int humidity;
    @XmlElement(name = "wind")
    private String wind;
    @XmlElement(name = "date")
    private String date;
    @XmlElement(name = "day")
    private String day;

    /**
     * Getter and Setter Methods for the data from the Weather website
     */
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getSkytext() {
        return skytext;
    }

    public void setSkytext(String skytext) {
        this.skytext = skytext;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    /**
     * The toString methods adds every data into a String
     *
     * @return string containing every data
     */
    @Override
    public String toString() {
        return "{location: " + location + ", temperature: " + temperature + ", skytext: " + skytext + ", humidity: " + humidity + ", wind: " + wind + ", date: " + date + ", day: " + day + "}";
    }
}
