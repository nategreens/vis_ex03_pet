package at.fhooe.mc;

import javax.xml.ws.Endpoint;

public class Main {
    /**
     * This class starts the server
     *
     * @author Patrick Felbauer - MC1510237003
     * @author Nathan Grinzinger - MC1510237043
     */
    public static void main(String[] args) {
        EnvironmentDataService service = new EnvironmentDataService();
        Endpoint endpoint = Endpoint.publish("http://localhost:8081/EnvironmentData", service);
        System.out.println("Server up and running...");
    }
}
