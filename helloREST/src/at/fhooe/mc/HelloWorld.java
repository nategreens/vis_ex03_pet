package at.fhooe.mc;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

/**
 * Created by Nathan Grinzinger on 19.01.2017.
 */
@Path("/oaschloch")
public class HelloWorld extends Application {
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getDataHTML() {
        return "<html><head></head><body>Hello World (HTML text)!!!</body></html>";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getDataPlain() {
        return "HelloWorld (plaintext)";
    }

    @GET
    @Path("/xml")
    @Produces(MediaType.TEXT_XML)
    public String getDataXML() {
        return "<HelloWorld>Hello World (XML text)!!!</HelloWorld>";
    }

    @GET
    @Path("/json")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataJSON() {
        return "{\"val\":\"HelloWorld\"}";
    }

}
