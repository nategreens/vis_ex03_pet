package at.fhooe.mc;

/**
 * This class contains the environmentService and methods to access it.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Path("/")
public class EnvironmentService extends Application {

    /**
     * This method prints an Overview-HTML which contains a link to the service itself
     *
     * @return the overview html
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getDataHTML() {
        return "<html>" +
                "<head></head>" +
                "<body>" +
                "<p><b>Overview</b></p>" +
                "<p><a href=\"/VIS_EnvData/environmentdataservice\">Environment Service</p>" +
                "</body>" +
                "</html>";
    }


    /**
     * This method prints links to all possible operations.
     *
     * @return links to all possible operations
     */
    @GET
    @Path("/environmentdataservice")
    @Produces(MediaType.TEXT_HTML)
    public String showOverviewHTML() {
        return "<html>" +
                "<head></head>" +
                "<body>" +
                "<p><b>Overview</b></p>" +
                "<p><a href=\"/VIS_EnvData/environmentdataservice/sensors\">Supported Sensors</p>" +
                "<p><a href=\"/VIS_EnvData/environmentdataservice/temperature\">Temperature Sensor</p>" +
                "<p><a href=\"/VIS_EnvData/environmentdataservice/humidity\">Humidity Sensor</p>" +
                "<p><a href=\"/VIS_EnvData/environmentdataservice/all\">All Sensors</p>" +
                "</body>" +
                "</html>";
    }

    /**
     * This method prints all supported sensors.
     *
     * @return all supported sensors
     */
    @GET
    @Path("/environmentdataservice/sensors")
    @Produces(MediaType.TEXT_XML)
    public String getSupportedSensors() {
        String[] envDataTypes = EnvService.getEnvDataTypes();
        String result = "<sensors>";
        for (int i = 0; i < envDataTypes.length; i++) {
            result += " <sensor>" + envDataTypes[i] + "</sensor>";
        }
        result += "</sensors>";
        return result;
    }

    /**
     * This method prints the humidity sensor data.
     *
     * @return a string containing the humidity sensor data
     */
    @GET
    @Path("/environmentdataservice/humidity")
    @Produces(MediaType.TEXT_XML)
    public String getHumidty() {
        EnvData data = EnvService.getHumidity();
        String result = "<data>" + data.toString() + "</data>";
        return result;
    }


    /**
     * This method prints the temperature sensor data.
     *
     * @return a String containing the temperature sensor data.
     */
    @GET
    @Path("/environmentdataservice/temperature")
    @Produces(MediaType.TEXT_XML)
    public String getTemperature() {
        EnvData data = EnvService.getTemperature();
        String result = "<data>" + data.toString() + "</data>";
        return result;
    }

    /**
     * This method prints all sensor data.
     *
     * @return a String containing all sensor data
     */
    @GET
    @Path("/environmentdataservice/all")
    @Produces(MediaType.TEXT_XML)
    public String getAllSensorData() {

        //get humidity sensor data
        EnvData humidity = EnvService.getHumidity();
        //get temperature sensor data
        EnvData temperature = EnvService.getTemperature();

        //compile into String
        StringBuilder builder = new StringBuilder();
        builder.append("<data>");
        builder.append("\n <sensor>" + humidity.toString() + "</sensor>");
        builder.append("\n <sensor>" + temperature.toString() + "</sensor>");
        builder.append("\n</data>");
        return builder.toString();
    }

}