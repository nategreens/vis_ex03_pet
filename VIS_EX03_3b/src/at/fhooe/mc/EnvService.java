package at.fhooe.mc;

/**
 * This class contains the EnvironmentService's functionality.
 *
 * @author Patrick Felbauer - MC1510237003
 * @author Nathan Grinzinger - MC1510237043
 */

import java.util.Random;

public class EnvService {


    public static String[] sensors = {"temperature", "humidity"};
    static EnvService envService = new EnvService();

    /**
     * This method returns the string array containing the sensor types.
     *
     * @return the string array containing the sensor types
     */
    public static String[] requestEnvironmentDataTypes() {
        return sensors;
    }

    /**
     * This method calls upon requestEnvironmentDataTypes().
     * Not sure why we have to call it this way, a friend told us it was supposed to look like this.
     *
     * @return a String array containing the sensor types
     */
    public static String[] getEnvDataTypes() {

        return envService.requestEnvironmentDataTypes();
    }

    /**
     * This method creates a new EnvData object to emulate SensorData.
     *
     * @param name The name of the emulated sensor.
     * @return an EnvData Object containing the name, a randomly generated value and the current time in milliseconds
     */
    public EnvData setEnvData(String name) {
        Random rand = new Random();
        int randomNum = rand.nextInt((10000 - 0) + 1) + 0;
        String value = String.valueOf(randomNum);
        long time = System.currentTimeMillis() / 1000;
        return new EnvData(name, value, time);
    }

    /**
     * This method returns a new EnvData object emulating a sensor of the type temperature.
     *
     * @return a new temperature sensor
     */
    private EnvData requestTemperature() {
        return setEnvData("temperature");
    }

    /**
     * This method calls upon requestTemperature().
     *
     * @return a new temperature sensor
     */
    public static EnvData getTemperature() {
        return envService.requestTemperature();
    }

    /**
     * This method returns a new EnvData object emulating a sensor of the type humidity.
     *
     * @return a new humidity sensor
     */
    private EnvData requestHumidity() {
        return setEnvData("humidity");
    }

    /**
     * This method calls upon requestHumidity().
     *
     * @return a new humidity sensor
     */
    public static EnvData getHumidity() {
        return envService.requestHumidity();
    }

}
