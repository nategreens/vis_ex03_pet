package at.fhooe.mc;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.rmi.RemoteException;

/**
 * Created by Felpower on 15.12.2016.
 */
@WebService
public interface IEnvService {
    /**
     * Liefert die Typen der zur Verfügung stehenden Umweltsensoren
     *
     * @return Ein String-Array mit den Typen der Umweltsensoren
     * @throws RemoteException Ein Fehler trat bei der Kommunikation auf
     * @see java.lang.String
     * @see java.rmi.RemoteException
     */
    @WebMethod
    public String[] requestEnvironmentDataTypes();

    /**
     * Liefert die Messwerte für einen speziellen Sensor in Form eines
     * Environment Data (EnvData) Objektes zurück
     *
     * @param _type der betreffende Umweltsensor
     * @return EnvData Die aktuellen Messwerte des entsprechenden Sensors
     * null, falls der Sensor nicht existiert
     * @throws RemoteException Ein Fehler trat bei der Kommunikation auf
     * @see java.lang.String
     * @see java.rmi.RemoteException
     */
    @WebMethod
    public EnvData requestEnvironmentData(String _type);


}
