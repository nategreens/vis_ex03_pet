/**
 * This Interface contains a String which is a sensor Name, a Value which is the value of the Sensor and a timestamp, which contains the current Time
 *
 * @author Patrick Felbauer  - 1510237003
 * @author Nathan Grinzinger - 1510237043
 */
package at.fhooe.mc;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@WebService
public class EnvData implements Serializable {

    @XmlElement(name = "cityName")
    private String cityName;
    @XmlElement(name = "val")
    private String val;
    @XmlElement(name = "timeStamp")
    private long timestamp;

    public EnvData() {

    }

    /**
     * The Constructor for the EnVDataClass
     *
     * @param name  The name of the Sensor
     * @param value The value of the sensor
     * @param time  The time of the creation of the EnvData object
     */

    public EnvData(String name, String value, long time) {
        timestamp = time;
        cityName = name;
        val = value;
    }

    /**
     * Puts the variables of the EnvData Interface into a String
     *
     * @return a String containing the time, name & value of the sensor separated by blank spaces.
     */
    @Override
    @WebMethod
    public String toString() {
        String envDataString = timestamp + " " + cityName + " " + val;
        return envDataString;
    }

    @WebMethod
    public String getCityName() {
        return this.cityName;
    }

    @WebMethod
    public String getValue() {
        return this.val;
    }

    @WebMethod
    public long getTimestamp() {
        return this.timestamp;
    }


}